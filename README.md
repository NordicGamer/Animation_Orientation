# Animation-Orientation
Small animation fix that reorient joints to match the target skeleton in Autodesk Maya.

# Youtube
[![Showcase](http://i.ytimg.com/vi/iubQ3cvsRAA/1.jpg)](https://www.youtube.com/watch?v=iubQ3cvsRAA&ab_channel=NordicGamer)
